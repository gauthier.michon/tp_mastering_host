# MICHON Gauthier B1A 09/05/2020

# Nom de la machines
echo "Nom de la machine :" [system.environment]::MachineName

# IP principale

"IP principale : "
Get-NetIPAddress -InterfaceIndex 21 | Format-Table

#OS

echo "OS : $env:OS"

echo "Version : $((Get-CimInstance Win32_OperatingSystem).version)"

# RAM

echo "Espace de la RAM utilisé"

echo "Espace libre de la RAM : $(Get-CimInstance Win32_PhysicalMemory | Measure-Object -Property capacity -Sum | Foreach {"{0:N2}" -f ([math]::round(($_.Sum / 1GB),2))})"

# Disque

echo "Espace disque utilise : "
gwmi win32_logicaldisk | Format-Table DeviceId, @{n="Size";e={[math]::Round($_.Size/1GB,2)}},@{n="UsedSpace";e={[math]::Round(($_.Size-$_.FreeSpace)/1GB,2)}}

echo "Espace disque dispo : "
gwmi win32_logicaldisk | Format-Table DeviceId, @{n="Size";e={[math]::Round($_.Size/1GB,2)}},@{n="FreeSpace";e={[math]::Round($_.FreeSpace/1GB,2)}}

# Utilisateurs

echo "Liste des utilisateurs : "
wmic useraccount list full

# On fait le ping

ping 8.8.8.8