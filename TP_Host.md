# Maîtrise de poste - Day 1

## Self-footprinting

### Host OS

Pour déterminer le nom de la machine on fait la commande```[system.environment]::MachineName``` et j'obtiens : 
```
LAPTOP-GAUTHIER
```

Pour déterminer l'OS, on fait la commande ```$env:OS``` : 
```
Windows_NT
```

Pour déterminer la version de l'OS on fait la commande ```$((Get-CimInstance Win32_OperatingSystem).version)``` et j'obtiens : 
```
10.0.18363
```

Mon OS est Windows et ma version est 10.0.18362.


Pour déterminer le nombres de bits du processeur, on fait la commande ```Get-WmiObject Win32_OperatingSystem | Select-Object  OSArchitecture```, et j'obtiens : 
```
64 bits
```

Pour déterminer le modèle du processeur, on fait la commande ```Get-CimInstance -ClassName Win32_ComputerSystem | Select-Object -Property SystemType``` et j'obtiens : 
```
SystemType
----------
x64-based PC
```

Pour déterminer la quantité RAM et modèle de la RAM, on fait la commande `Get-CimInstance win32_physicalmemory | Format-Table Manufacturer,Banklabel,Configuredclockspeed,Devicelocator,Capacity,Serialnumber -autosize`, et j'obtiens : 
```
Manufacturer Banklabel Configuredclockspeed Devicelocator          Capacity Serialnumber
------------ --------- -------------------- -------------          -------- ------------
Samsung      BANK 0                    2133 Bottom-Slot 1(left)  4294967296 15101987
Samsung      BANK 2                    2133 Bottom-Slot 2(right) 4294967296 15101995
```
Ici ma RAM est une samsung et possède une capacité de 4go.


### Devices

#### Trouver

Pour le processeur, on fait la commande ```Get-ComputerInfo -Property "*proc*"```, et j'obtiens :

```
CsNumberOfLogicalProcessors : 8
CsNumberOfProcessors        : 1
CsProcessors                : {Intel(R) Core(TM) i7-6700HQ CPU @ 2.60GHz}
OsMaxNumberOfProcesses      : 4294967295
OsMaxProcessMemorySize      : 137438953344
OsNumberOfProcesses         : 265
```

Mon processeur est un Intel Core i7.
Le 6 correspond à la génération du processeur.
Le 700 est une chaîne de chiffres utilisés par le fabricant pour identifier son produit.
Le HQ correspond à une qualité graphiques de hautes performances, et ayant quatre cœurs.


Pour le modèle et la marque du disque dur : 

`Get-CimInstance -ClassName Win32_Processor | Select-Object -ExcludeProperty "CIM*"`

```
DeviceID Name                                      Caption                              MaxClockSpeed SocketDesignation
-------- ----                                      -------                              ------------- -----------------
CPU0     Intel(R) Core(TM) i7-6700HQ CPU @ 2.60GHz Intel64 Family 6 Model 94 Stepping 3 2592          U3E1
```
Ici c'est une <strong>Intel64 Family 6 Model 94 Stepping 3</strong>

#### Disque dur

Pour identifier les différentes partitions de votre/vos disque(s) dur(s), on fait : `Get-Partition` et j'obtiens : 

```
   DiskPath :
\\?\scsi#disk&ven_sandisk&prod_sd8snat-128g-100#4&2f87019f&0&000000#{53f56307-b6bf-11d0-94f2-00a0c91efb8b}

PartitionNumber  DriveLetter Offset                                        Size Type
---------------  ----------- ------                                        ---- ----
1                            1048576                                     260 MB System
2                            273678336                                    16 MB Reserved
3                C           290455552                                118.01 GB Basic
4                            127002476544                                980 MB Recovery
```

La partition 1 héberge le système d'exploitation
La partition 2, je ne sais pas
La partition 3 stock les installation sur le pc
La partition 4 fait des sauvegardes de la machine récupérables

### Network

Pour afficher la liste des cartes réseau de votre machine, on fait la commande : `Get-NetAdapter | fl Name, InterfaceIndex`

```
Name           : VirtualBox Host-Only Network #5
InterfaceIndex : 25

Name           : VirtualBox Host-Only Network #2
InterfaceIndex : 23

Name           : Wi-Fi
InterfaceIndex : 21

Name           : VirtualBox Host-Only Network
InterfaceIndex : 20

Name           : VirtualBox Host-Only Network #3
InterfaceIndex : 18

Name           : VMware Network Adapter VMnet1
InterfaceIndex : 10

Name           : Ethernet 2
InterfaceIndex : 9

Name           : Connexion réseau Bluetooth
InterfaceIndex : 6

Name           : VMware Network Adapter VMnet8
InterfaceIndex : 4
```

Les interfaces 18, 20, 23 et 25, servent aux VM dans VirtualBox
Les interfaces 4 et 10, servent aux VM dans VMWare
L'interface 21 permet d'avoir la Wi-Fi
L'interface 6 permet d'avoir le bluetooth
L'interface 9 permet une connexion Ethernet



Pour lister tous les ports TCP et UDP en utilisation, on fait la commande `netstat -abo` et j'obtiens : 

```
Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    0.0.0.0:80             Laptop-Gauthier:0      LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:135            Laptop-Gauthier:0      LISTENING       416
  RpcSs
 [svchost.exe]
  TCP    0.0.0.0:445            Laptop-Gauthier:0      LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:902            Laptop-Gauthier:0      LISTENING       6428
 [vmware-authd.exe]
  TCP    0.0.0.0:912            Laptop-Gauthier:0      LISTENING       6428
 [vmware-authd.exe]
  TCP    0.0.0.0:3306           Laptop-Gauthier:0      LISTENING       8808
 [mysqld.exe]
  TCP    0.0.0.0:5040           Laptop-Gauthier:0      LISTENING       5580
  CDPSvc
 [svchost.exe]
  TCP    0.0.0.0:33060          Laptop-Gauthier:0      LISTENING       8808
 [mysqld.exe]
  TCP    0.0.0.0:49664          Laptop-Gauthier:0      LISTENING       748
 [lsass.exe]
  TCP    0.0.0.0:49665          Laptop-Gauthier:0      LISTENING       616
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:49666          Laptop-Gauthier:0      LISTENING       1668
  Schedule
 [svchost.exe]
  TCP    0.0.0.0:49667          Laptop-Gauthier:0      LISTENING       2592
  EventLog
 [svchost.exe]
  TCP    0.0.0.0:49668          Laptop-Gauthier:0      LISTENING       3512
  SessionEnv
 [svchost.exe]
  TCP    0.0.0.0:49669          Laptop-Gauthier:0      LISTENING       4716
 [spoolsv.exe]
  TCP    0.0.0.0:49670          Laptop-Gauthier:0      LISTENING       5296
  PolicyAgent
 [svchost.exe]
  TCP    0.0.0.0:49700          Laptop-Gauthier:0      LISTENING       696
 Impossible d’obtenir les informations de propriétaire
  TCP    10.4.1.1:139           Laptop-Gauthier:0      LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    10.4.2.1:139           Laptop-Gauthier:0      LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:5354         Laptop-Gauthier:0      LISTENING       5876
 [mDNSResponder.exe]
  TCP    127.0.0.1:6463         Laptop-Gauthier:0      LISTENING       15428
 [Discord.exe]
  TCP    127.0.0.1:8092         Laptop-Gauthier:0      LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:9990         Laptop-Gauthier:0      LISTENING       6192
 [NvNetworkService.exe]
  TCP    127.0.0.1:10143        Laptop-Gauthier:0      LISTENING       14764
 [node.exe]
  TCP    127.0.0.1:15292        Laptop-Gauthier:0      LISTENING       14508
 [Adobe Desktop Service.exe]
  TCP    127.0.0.1:15393        Laptop-Gauthier:0      LISTENING       14508
 [Adobe Desktop Service.exe]
  TCP    127.0.0.1:16494        Laptop-Gauthier:0      LISTENING       14508
 [Adobe Desktop Service.exe]
  TCP    127.0.0.1:45623        Laptop-Gauthier:0      LISTENING       14764
 [node.exe]
  TCP    127.0.0.1:49674        Laptop-Gauthier:49675  ESTABLISHED     8808
 [mysqld.exe]
  TCP    127.0.0.1:49675        Laptop-Gauthier:49674  ESTABLISHED     8808
 [mysqld.exe]
  TCP    127.0.0.1:49751        Laptop-Gauthier:0      LISTENING       14764
 [node.exe]
  TCP    127.0.0.1:49751        Laptop-Gauthier:49854  ESTABLISHED     14764
 [node.exe]
  TCP    127.0.0.1:49763        Laptop-Gauthier:0      LISTENING       14764
 [node.exe]
  TCP    127.0.0.1:49854        Laptop-Gauthier:49751  ESTABLISHED     14492
 [Adobe CEF Helper.exe]
  TCP    192.168.1.18:139       Laptop-Gauthier:0      LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.1.18:49683     40.67.251.132:https    ESTABLISHED     6468
  WpnService
 [svchost.exe]
  TCP    192.168.1.18:49808     52.114.75.156:https    ESTABLISHED     16120
 [Teams.exe]
  TCP    192.168.1.18:50270     ec2-63-32-210-14:https  ESTABLISHED     16300
 [CoreSync.exe]
  TCP    192.168.1.18:50324     52.114.88.23:https     ESTABLISHED     13376
 [Teams.exe]
  TCP    192.168.1.18:50329     ec2-52-1-27-106:https  ESTABLISHED     16300
 [CoreSync.exe]
  TCP    192.168.1.18:50747     162.159.136.234:https  ESTABLISHED     744
 [Discord.exe]
  TCP    192.168.1.18:50755     a23-215-188-129:http   TIME_WAIT       0
  TCP    192.168.1.18:50756     25:https               TIME_WAIT       0
  TCP    192.168.1.18:50757     91.228.74.152:https    ESTABLISHED     452
 [chrome.exe]
  TCP    192.168.1.18:50758     amidt:https            CLOSE_WAIT      452
 [chrome.exe]
  TCP    192.168.1.18:50759     amidt:https            CLOSE_WAIT      452
 [chrome.exe]
  TCP    192.168.1.18:50760     80.252.91.53:https     ESTABLISHED     452
 [chrome.exe]
  TCP    192.168.1.18:50761     80.252.91.53:https     ESTABLISHED     452
 [chrome.exe]
  TCP    192.168.1.18:50762     a23-210-43-4:https     ESTABLISHED     452
 [chrome.exe]
  TCP    192.168.1.18:50763     213.254.244.12:https   CLOSE_WAIT      452
 [chrome.exe]
  TCP    192.168.1.18:50764     213.254.244.12:https   CLOSE_WAIT      452
 [chrome.exe]
  TCP    192.168.1.18:50765     213.254.244.12:https   CLOSE_WAIT      452
 [chrome.exe]
  TCP    192.168.1.18:50766     ec2-3-224-185-234:https  ESTABLISHED     452
 [chrome.exe]
  TCP    192.168.1.18:50771     151.101.122.217:https  ESTABLISHED     452
 [chrome.exe]
  TCP    192.168.1.18:50773     40.77.226.250:https    ESTABLISHED     452
 [chrome.exe]
  TCP    192.168.1.18:50775     40.77.226.250:https    ESTABLISHED     452
 [chrome.exe]
  TCP    192.168.1.18:50776     40.77.226.250:https    ESTABLISHED     452
 [chrome.exe]
  TCP    192.168.1.18:50778     20.190.137.69:https    ESTABLISHED     13376
 [Teams.exe]
  TCP    192.168.1.18:50781     52.114.132.34:https    ESTABLISHED     13376
 [Teams.exe]
  TCP    192.168.1.18:50782     52.114.75.17:https     ESTABLISHED     13376
 [Teams.exe]
  TCP    192.168.16.1:139       Laptop-Gauthier:0      LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.26.1:139       Laptop-Gauthier:0      LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.68.1:139       Laptop-Gauthier:0      LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.182.1:139      Laptop-Gauthier:0      LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:80                Laptop-Gauthier:0      LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:135               Laptop-Gauthier:0      LISTENING       416
  RpcSs
 [svchost.exe]
  TCP    [::]:445               Laptop-Gauthier:0      LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:3306              Laptop-Gauthier:0      LISTENING       8808
 [mysqld.exe]
  TCP    [::]:33060             Laptop-Gauthier:0      LISTENING       8808
 [mysqld.exe]
  TCP    [::]:49664             Laptop-Gauthier:0      LISTENING       748
 [lsass.exe]
  TCP    [::]:49665             Laptop-Gauthier:0      LISTENING       616
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:49666             Laptop-Gauthier:0      LISTENING       1668
  Schedule
 [svchost.exe]
  TCP    [::]:49667             Laptop-Gauthier:0      LISTENING       2592
  EventLog
 [svchost.exe]
  TCP    [::]:49668             Laptop-Gauthier:0      LISTENING       3512
  SessionEnv
 [svchost.exe]
  TCP    [::]:49669             Laptop-Gauthier:0      LISTENING       4716
 [spoolsv.exe]
  TCP    [::]:49670             Laptop-Gauthier:0      LISTENING       5296
  PolicyAgent
 [svchost.exe]
  TCP    [::]:49700             Laptop-Gauthier:0      LISTENING       696
 Impossible d’obtenir les informations de propriétaire
  TCP    [::1]:50049            Laptop-Gauthier:0      LISTENING       10616
 [jhi_service.exe]
  TCP    [2a01:cb19:143:4300:b538:4f58:9452:6f93]:50285  [2406:da14:88d:a100:ead4:d05:c10e:4c0a]:https  ESTABLISHED     452
 [chrome.exe]
  TCP    [2a01:cb19:143:4300:b538:4f58:9452:6f93]:50292  [2406:da14:88d:a102:4bf3:be8d:b309:114f]:https  ESTABLISHED     452
 [chrome.exe]
  TCP    [2a01:cb19:143:4300:b538:4f58:9452:6f93]:50319  ws-in-xbc:5228         ESTABLISHED     452
 [chrome.exe]
  TCP    [2a01:cb19:143:4300:b538:4f58:9452:6f93]:50777  [2606:4700:20::681a:6c2]:https  ESTABLISHED     452
 [chrome.exe]
  TCP    [2a01:cb19:143:4300:b538:4f58:9452:6f93]:50779  [2620:1ec:42::132]:https  ESTABLISHED     13376
 [Teams.exe]
  TCP    [2a01:cb19:143:4300:b538:4f58:9452:6f93]:50780  [2620:1ec:c::11]:https  ESTABLISHED     13376
 [Teams.exe]
  TCP    [2a01:cb19:143:4300:b538:4f58:9452:6f93]:50785  [2406:da14:88d:a102:4bf3:be8d:b309:114f]:https  ESTABLISHED     452
 [chrome.exe]
  TCP    [2a01:cb19:143:4300:b538:4f58:9452:6f93]:50787  [2406:da14:88d:a102:4bf3:be8d:b309:114f]:https  ESTABLISHED     452
```

Le processus Windows svchost.exe sert à lancer tous les autres processus.
Il y a ensuite tous les ports utilisé par des applis comme teams ou chrome.


### Users


Pour la liste des utilisateurs complet de la machine, on fait : 
```
PS C:\Users\Gauthier> wmic useraccount list full


AccountType=512
Description=Compte d'utilisateur d'administration
Disabled=TRUE
Domain=LAPTOP-GAUTHIER
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=Administrateur
PasswordChangeable=TRUE
PasswordExpires=FALSE
PasswordRequired=FALSE
SID=S-1-5-21-4084104036-22647758-804814827-500
SIDType=1
Status=Degraded


AccountType=512
Description=Compte utilisateur géré par le système.
Disabled=TRUE
Domain=LAPTOP-GAUTHIER
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=DefaultAccount
PasswordChangeable=TRUE
PasswordExpires=FALSE
PasswordRequired=FALSE
SID=S-1-5-21-4084104036-22647758-804814827-503
SIDType=1
Status=Degraded


AccountType=512
Description=
Disabled=FALSE
Domain=LAPTOP-GAUTHIER
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=Gauthier
PasswordChangeable=TRUE
PasswordExpires=FALSE
PasswordRequired=FALSE
SID=S-1-5-21-4084104036-22647758-804814827-1001
SIDType=1
Status=OK


AccountType=512
Description=Compte d'utilisateur invité
Disabled=TRUE
Domain=LAPTOP-GAUTHIER
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=Invité
PasswordChangeable=FALSE
PasswordExpires=FALSE
PasswordRequired=FALSE
SID=S-1-5-21-4084104036-22647758-804814827-501
SIDType=1
Status=Degraded


AccountType=512
Description=Compte d'utilisateur géré et utilisé par le système pour les scénarios Windows Defender Application Guard.
Disabled=TRUE
Domain=LAPTOP-GAUTHIER
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=WDAGUtilityAccount
PasswordChangeable=TRUE
PasswordExpires=TRUE
PasswordRequired=TRUE
SID=S-1-5-21-4084104036-22647758-804814827-504
SIDType=1
Status=Degraded


AccountType=512
Description=Local user account for execution of R scripts in SQL Server instance YNOV
Disabled=FALSE
Domain=LAPTOP-GAUTHIER
FullName=YNOV01
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=YNOV01
PasswordChangeable=FALSE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-4084104036-22647758-804814827-1006
SIDType=1
Status=OK


AccountType=512
Description=Local user account for execution of R scripts in SQL Server instance YNOV
Disabled=FALSE
Domain=LAPTOP-GAUTHIER
FullName=YNOV02
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=YNOV02
PasswordChangeable=FALSE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-4084104036-22647758-804814827-1007
SIDType=1
Status=OK


AccountType=512
Description=Local user account for execution of R scripts in SQL Server instance YNOV
Disabled=FALSE
Domain=LAPTOP-GAUTHIER
FullName=YNOV03
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=YNOV03
PasswordChangeable=FALSE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-4084104036-22647758-804814827-1008
SIDType=1
Status=OK


AccountType=512
Description=Local user account for execution of R scripts in SQL Server instance YNOV
Disabled=FALSE
Domain=LAPTOP-GAUTHIER
FullName=YNOV04
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=YNOV04
PasswordChangeable=FALSE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-4084104036-22647758-804814827-1009
SIDType=1
Status=OK


AccountType=512
Description=Local user account for execution of R scripts in SQL Server instance YNOV
Disabled=FALSE
Domain=LAPTOP-GAUTHIER
FullName=YNOV05
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=YNOV05
PasswordChangeable=FALSE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-4084104036-22647758-804814827-1010
SIDType=1
Status=OK


AccountType=512
Description=Local user account for execution of R scripts in SQL Server instance YNOV
Disabled=FALSE
Domain=LAPTOP-GAUTHIER
FullName=YNOV06
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=YNOV06
PasswordChangeable=FALSE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-4084104036-22647758-804814827-1011
SIDType=1
Status=OK


AccountType=512
Description=Local user account for execution of R scripts in SQL Server instance YNOV
Disabled=FALSE
Domain=LAPTOP-GAUTHIER
FullName=YNOV07
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=YNOV07
PasswordChangeable=FALSE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-4084104036-22647758-804814827-1012
SIDType=1
Status=OK


AccountType=512
Description=Local user account for execution of R scripts in SQL Server instance YNOV
Disabled=FALSE
Domain=LAPTOP-GAUTHIER
FullName=YNOV08
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=YNOV08
PasswordChangeable=FALSE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-4084104036-22647758-804814827-1013
SIDType=1
Status=OK


AccountType=512
Description=Local user account for execution of R scripts in SQL Server instance YNOV
Disabled=FALSE
Domain=LAPTOP-GAUTHIER
FullName=YNOV09
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=YNOV09
PasswordChangeable=FALSE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-4084104036-22647758-804814827-1014
SIDType=1
Status=OK


AccountType=512
Description=Local user account for execution of R scripts in SQL Server instance YNOV
Disabled=FALSE
Domain=LAPTOP-GAUTHIER
FullName=YNOV10
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=YNOV10
PasswordChangeable=FALSE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-4084104036-22647758-804814827-1015
SIDType=1
Status=OK


AccountType=512
Description=Local user account for execution of R scripts in SQL Server instance YNOV
Disabled=FALSE
Domain=LAPTOP-GAUTHIER
FullName=YNOV11
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=YNOV11
PasswordChangeable=FALSE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-4084104036-22647758-804814827-1016
SIDType=1
Status=OK


AccountType=512
Description=Local user account for execution of R scripts in SQL Server instance YNOV
Disabled=FALSE
Domain=LAPTOP-GAUTHIER
FullName=YNOV12
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=YNOV12
PasswordChangeable=FALSE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-4084104036-22647758-804814827-1017
SIDType=1
Status=OK


AccountType=512
Description=Local user account for execution of R scripts in SQL Server instance YNOV
Disabled=FALSE
Domain=LAPTOP-GAUTHIER
FullName=YNOV13
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=YNOV13
PasswordChangeable=FALSE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-4084104036-22647758-804814827-1018
SIDType=1
Status=OK


AccountType=512
Description=Local user account for execution of R scripts in SQL Server instance YNOV
Disabled=FALSE
Domain=LAPTOP-GAUTHIER
FullName=YNOV14
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=YNOV14
PasswordChangeable=FALSE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-4084104036-22647758-804814827-1019
SIDType=1
Status=OK


AccountType=512
Description=Local user account for execution of R scripts in SQL Server instance YNOV
Disabled=FALSE
Domain=LAPTOP-GAUTHIER
FullName=YNOV15
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=YNOV15
PasswordChangeable=FALSE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-4084104036-22647758-804814827-1020
SIDType=1
Status=OK


AccountType=512
Description=Local user account for execution of R scripts in SQL Server instance YNOV
Disabled=FALSE
Domain=LAPTOP-GAUTHIER
FullName=YNOV16
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=YNOV16
PasswordChangeable=FALSE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-4084104036-22647758-804814827-1021
SIDType=1
Status=OK


AccountType=512
Description=Local user account for execution of R scripts in SQL Server instance YNOV
Disabled=FALSE
Domain=LAPTOP-GAUTHIER
FullName=YNOV17
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=YNOV17
PasswordChangeable=FALSE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-4084104036-22647758-804814827-1022
SIDType=1
Status=OK


AccountType=512
Description=Local user account for execution of R scripts in SQL Server instance YNOV
Disabled=FALSE
Domain=LAPTOP-GAUTHIER
FullName=YNOV18
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=YNOV18
PasswordChangeable=FALSE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-4084104036-22647758-804814827-1023
SIDType=1
Status=OK


AccountType=512
Description=Local user account for execution of R scripts in SQL Server instance YNOV
Disabled=FALSE
Domain=LAPTOP-GAUTHIER
FullName=YNOV19
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=YNOV19
PasswordChangeable=FALSE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-4084104036-22647758-804814827-1024
SIDType=1
Status=OK


AccountType=512
Description=Local user account for execution of R scripts in SQL Server instance YNOV
Disabled=FALSE
Domain=LAPTOP-GAUTHIER
FullName=YNOV20
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=YNOV20
PasswordChangeable=FALSE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-4084104036-22647758-804814827-1025
SIDType=1
Status=OK
```

Le nom de l'utilisateur qui est full admin sur la machine est Administrateur.

### Processus

Pour déterminer la liste des processus de la machine, on fait la commande    `Get-Service`, j'obtiens : 

```
Status   Name               DisplayName
------   ----               -----------
Stopped  AarSvc_436cd       AarSvc_436cd
Running  AdobeUpdateService AdobeUpdateService
Running  AGMService         Adobe Genuine Monitor Service
Running  AGSService         Adobe Genuine Software Integrity Se...
Stopped  AJRouter           Service de routeur AllJoyn
Stopped  ALG                Service de la passerelle de la couc...
Stopped  AntiVirMailService Avira Protection e-mail
Running  AntivirProtecte... Avira Service protégé
Running  AntiVirSchedule... Avira Planificateur
Running  AntiVirService     Avira Protection temps réel
Stopped  AntiVirWebService  Avira Protection Web
Running  AppHostSvc         Application Host Helper Service
Stopped  AppIDSvc           Identité de l’application
Running  Appinfo            Informations d’application
Stopped  AppReadiness       Préparation des applications
Stopped  AppXSvc            Service de déploiement AppX (AppXSVC)
Stopped  aspnet_state       ASP.NET State Service
Running  AudioEndpointBu... Générateur de points de terminaison...
Running  Audiosrv           Audio Windows
Stopped  autotimesvc        Heure cellulaire
Running  Avira.ServiceHost  Avira Service Host
Stopped  AxInstSV           Programme d’installation ActiveX (A...
Stopped  BcastDVRUserSer... BcastDVRUserService_436cd
Stopped  BDESVC             Service de chiffrement de lecteur B...
Stopped  BEService          BattlEye Service
Running  BFE                Moteur de filtrage de base
Running  BITS               Service de transfert intelligent en...
Stopped  BluetoothUserSe... BluetoothUserService_436cd
Running  Bonjour Service    Service Bonjour
Running  BrokerInfrastru... Service d’infrastructure des tâches...
Running  BTAGService        Service de passerelle audio Bluetooth
Running  BthAvctpSvc        Service AVCTP
Running  bthserv            Service de prise en charge Bluetooth
Running  camsvc             Service Gestionnaire d’accès aux fo...
Stopped  CaptureService_... CaptureService_436cd
Running  cbdhsvc_436cd      cbdhsvc_436cd
Running  CDPSvc             Service de plateforme des appareils...
Running  CDPUserSvc_436cd   CDPUserSvc_436cd
Running  CertPropSvc        Propagation du certificat
Running  ClickToRunSvc      Microsoft Office Click-to-Run Service
Stopped  ClipSVC            Service de licences de client (Clip...
Stopped  COMSysApp          Application système COM+
Stopped  ConsentUxUserSv... ConsentUxUserSvc_436cd
Running  CoreMessagingRe... CoreMessaging
Running  cphs               Intel(R) Content Protection HECI Se...
Running  cplspcon           Intel(R) Content Protection HDCP Se...
Stopped  CredentialEnrol... CredentialEnrollmentManagerUserSvc_...
Running  CryptSvc           Services de chiffrement
Stopped  dbupdate           Service Mise à jour Dropbox (dbupdate)
Stopped  dbupdatem          Service Mise à jour Dropbox (dbupda...
Running  DcomLaunch         Lanceur de processus serveur DCOM
Stopped  defragsvc          Optimiser les lecteurs
Stopped  DeviceAssociati... DeviceAssociationBrokerSvc_436cd
Running  DeviceAssociati... Service d’association de périphérique
Stopped  DeviceInstall      Service d’installation de périphérique
Stopped  DevicePickerUse... DevicePickerUserSvc_436cd
Stopped  DevicesFlowUser... DevicesFlowUserSvc_436cd
Stopped  DevQueryBroker     Service Broker de découverte en arr...
Running  Dhcp               Client DHCP
Stopped  diagnosticshub.... Service Collecteur standard du conc...
Stopped  diagsvc            Diagnostic Execution Service
Running  DiagTrack          Expériences des utilisateurs connec...
Running  DispBrokerDeskt... Service de stratégie d'affichage
Running  DisplayEnhancem... Service d'amélioration de l'affichage
Stopped  DmEnrollmentSvc    Service d'inscription de la gestion...
Stopped  dmwappushservice   Service de routage de messages Push...
Running  Dnscache           Client DNS
Stopped  DoSvc              Optimisation de livraison
Stopped  dot3svc            Configuration automatique de réseau...
Running  DPS                Service de stratégie de diagnostic
Stopped  DsmSvc             Gestionnaire d’installation de péri...
Stopped  DsSvc              Service de partage des données
Running  DusmSvc            Consommation des données
Stopped  Eaphost            Protocole EAP (Extensible Authentic...
Stopped  EasyAntiCheat      EasyAntiCheat
Stopped  EFS                Système de fichiers EFS (Encrypting...
Stopped  embeddedmode       Mode incorporé
Stopped  EntAppSvc          Service de gestion des applications...
Running  esifsvc            ESIF Upper Framework Service
Running  EventLog           Journal d’événements Windows
Running  EventSystem        Système d’événement COM+
Running  EvtEng             Intel(R) PROSet/Wireless Event Log
Stopped  Fax                Télécopie
Stopped  fdPHost            Hôte du fournisseur de découverte d...
Stopped  FDResPub           Publication des ressources de décou...
Stopped  fhsvc              Service d’historique des fichiers
Running  FontCache          Service de cache de police Windows
Running  FontCache3.0.0.0   Cache de police de Windows Presenta...
Stopped  FrameServer        Serveur de trame de la Caméra Windows
Running  GfExperienceSer... NVIDIA GeForce Experience Service
Stopped  GoogleChromeEle... Google Chrome Elevation Service
Running  gpsvc              Client de stratégie de groupe
Stopped  GraphicsPerfSvc    GraphicsPerfSvc
Stopped  gupdate            Service Google Update (gupdate)
Stopped  gupdatem           Service Google Update (gupdatem)
Stopped  hidserv            Service du périphérique d’interface...
Running  HP Comm Recover    HP Comm Recovery
Running  hpqwmiex           HP Software Framework Service
Running  HPSupportSoluti... HP Support Solutions Framework Service
Running  HPWMISVC           HPWMISVC
Stopped  HvHost             Service d'hôte HV
Running  IAStorDataMgrSvc   Intel(R) Rapid Storage Technology
Running  ibtsiva            Intel Bluetooth Service
Stopped  icssvc             Service Point d'accès sans fil mobi...
Running  igfxCUIService2... Intel(R) HD Graphics Control Panel ...
Running  IKEEXT             Modules de génération de clés IKE e...
Running  InstallService     Installation du service Microsoft S...
Stopped  Intel(R) Capabi... Intel(R) Capability Licensing Servi...
Stopped  Intel(R) WiDi SAM  Intel(R) WiDi Software Asset Manager
Running  iphlpsvc           Assistance IP
Stopped  IpxlatCfgSvc       Service de configuration de convers...
Running  jhi_service        Intel(R) Dynamic Application Loader...
Running  KeyIso             Isolation de clé CNG
Stopped  KtmRm              Service KtmRm pour Distributed Tran...
Running  LanmanServer       Serveur
Running  LanmanWorkstation  Station de travail
Stopped  lfsvc              Service de géolocalisation
Running  LicenseManager     Serveur Gestionnaire de licences Wi...
Stopped  lltdsvc            Mappage de découverte de topologie ...
Running  lmhosts            Assistance NetBIOS sur TCP/IP
Running  LSM                Gestionnaire de session locale
Stopped  LxpSvc             Service d'expérience linguistique
Stopped  MapsBroker         Gestionnaire des cartes téléchargées
Stopped  MessagingServic... MessagingService_436cd
Stopped  MixedRealityOpe... Windows Mixed Reality OpenXR Service
Running  mpssvc             Pare-feu Windows Defender
Stopped  MSDTC              Coordinateur de transactions distri...
Stopped  MSiSCSI            Service Initiateur iSCSI de Microsoft
Stopped  msiserver          Windows Installer
Stopped  MsMpiLaunchSvc     MS-MPI Launch Service
Running  MSSQL$YNOV         SQL Server (YNOV)
Running  MSSQLFDLauncher... SQL Full-text Filter Daemon Launche...
Running  MSSQLLaunchpad$... SQL Server Launchpad (YNOV)
Running  MySQL80            MySQL80
Stopped  MyWiFiDHCPDNS      Wireless PAN DHCP Server
Stopped  NaturalAuthenti... Authentification naturelle
Stopped  NcaSvc             Assistant Connectivité réseau
Running  NcbService         Service Broker pour les connexions ...
Stopped  NcdAutoSetup       Configuration automatique des périp...
Stopped  Netlogon           Netlogon
Running  Netman             Connexions réseau
Running  netprofm           Service Liste des réseaux
Stopped  NetSetupSvc        Service Configuration du réseau
Stopped  NetTcpPortSharing  Service de partage de ports Net.Tcp
Running  NgcCtnrSvc         Conteneur Microsoft Passport
Running  NgcSvc             Microsoft Passport
Running  NlaSvc             Connaissance des emplacements réseau
Running  nsi                Service Interface du magasin réseau
Running  NVDisplay.Conta... NVIDIA Display Container LS
Running  NvNetworkService   NVIDIA Network Service
Stopped  NvStreamSvc        NVIDIA Streamer Service
Running  OneSyncSvc_436cd   OneSyncSvc_436cd
Stopped  ose                Office  Source Engine
Running  osrss              Service de simplification des mises...
Stopped  p2pimsvc           Gestionnaire d’identité réseau homo...
Stopped  p2psvc             Groupement de mise en réseau de pairs
Running  PcaSvc             Service de l’Assistant Compatibilit...
Stopped  perceptionsimul... Service de simulation de perception...
Stopped  PerfHost           Hôte de DLL de compteur de performance
Running  PhoneSvc           Service téléphonique
Stopped  PimIndexMainten... PimIndexMaintenanceSvc_436cd
Stopped  pla                Journaux & alertes de performance
Running  PlugPlay           Plug-and-Play
Stopped  PNRPAutoReg        Service de publication des noms d’o...
Stopped  PNRPsvc            Protocole PNRP
Running  PolicyAgent        Agent de stratégie IPsec
Running  Power              Alimentation
Stopped  PrintNotify        Extensions et notifications des imp...
Stopped  PrintWorkflowUs... PrintWorkflowUserSvc_436cd
Running  ProfSvc            Service de profil utilisateur
Stopped  PushToInstall      Service PushToInstall de Windows
Stopped  QWAVE              Expérience audio-vidéo haute qualit...
Stopped  RasAuto            Gestionnaire des connexions automat...
Running  RasMan             Gestionnaire des connexions d’accès...
Running  RegSrvc            Intel(R) PROSet/Wireless Registry S...
Running  RemoteAccess       Routage et accès distant
Stopped  RemoteRegistry     Registre à distance
Running  ReportServer$YNOV  SQL Server Reporting Services (YNOV)
Stopped  RetailDemo         Service de démo du magasin
Stopped  RmSvc              Service de gestion radio
Running  RpcEptMapper       Mappeur de point de terminaison RPC
Stopped  RpcLocator         Localisateur d’appels de procédure ...
Running  RpcSs              Appel de procédure distante (RPC)
Running  RtkAudioService    Realtek Audio Service
Running  SamSs              Gestionnaire de comptes de sécurité
Stopped  SCardSvr           Carte à puce
Stopped  ScDeviceEnum       Service d’énumération de périphériq...
Running  Schedule           Planificateur de tâches
Stopped  SCPolicySvc        Stratégie de retrait de la carte à ...
Stopped  SDRSVC             Sauvegarde Windows
Running  seclogon           Ouverture de session secondaire
Running  SecurityHealthS... Service Sécurité Windows
Stopped  SEMgrSvc           Gestionnaires des paiements et des ...
Running  SENS               Service de notification d’événement...
Stopped  SensorDataService  Service Données de capteur
Stopped  SensorService      Service de capteur
Stopped  SensrSvc           Service de surveillance des capteurs
Running  SessionEnv         Configuration des services Bureau à...
Running  SgrmBroker         Service Broker du moniteur d'exécut...
Stopped  SharedAccess       Partage de connexion Internet (ICS)
Stopped  SharedRealitySvc   Service de données spatiales
Running  ShellHWDetection   Détection matériel noyau
Stopped  shpamsvc           Shared PC Account Manager
Stopped  smphost            SMP de l’Espace de stockages Microsoft
Stopped  SmsRouter          Service Routeur SMS Microsoft Windows.
Stopped  SNMPTRAP           Interruption SNMP
Stopped  spectrum           Service de perception Windows
Running  Spooler            Spouleur d’impression
Running  sppsvc             Protection logicielle
Stopped  SQLAgent$YNOV      SQL Server Agent (YNOV)
Stopped  SQLBrowser         SQL Server Browser
Running  SQLTELEMETRY$YNOV  SQL Server CEIP service (YNOV)
Running  SQLWriter          Enregistreur VSS SQL Server
Running  SSDPSRV            Découverte SSDP
Stopped  ssh-agent          OpenSSH Authentication Agent
Running  SstpSvc            Service SSTP (Secure Socket Tunneli...
Running  StateRepository    Service State Repository (StateRepo...
Stopped  Steam Client Se... Steam Client Service
Stopped  stisvc             Acquisition d’image Windows (WIA)
Running  StorSvc            Service de stockage
Stopped  svsvc              Vérificateur de points
Stopped  swprv              Fournisseur de cliché instantané de...
Running  SynTPEnhService    SynTPEnh Caller Service
Running  SysMain            SysMain
Running  SystemEventsBroker Service Broker des événements système
Running  TabletInputService Service du clavier tactile et du vo...
Running  TapiSrv            Téléphonie
Running  TermService        Services Bureau à distance
Running  Themes             Thèmes
Stopped  TieringEngineSe... Gestion des niveaux de stockage
Running  TimeBrokerSvc      Service Broker pour les événements ...
Running  TokenBroker        Gestionnaire de comptes web
Running  TrkWks             Client de suivi de lien distribué
Stopped  TroubleshootingSvc Service de résolution des problèmes...
Stopped  TrustedInstaller   Programme d’installation pour les m...
Stopped  tzautoupdate       Programme de mise à jour automatiqu...
Stopped  UmRdpService       Redirecteur de port du mode utilisa...
Stopped  UnistoreSvc_436cd  UnistoreSvc_436cd
Stopped  upnphost           Hôte de périphérique UPnP
Stopped  UserDataSvc_436cd  UserDataSvc_436cd
Running  UserManager        Gestionnaire des utilisateurs
Running  UsoSvc             Mettre à jour le service Orchestrator
Stopped  VacSvc             Service de composition audio volumé...
Running  VaultSvc           Gestionnaire d’informations d’ident...
Stopped  VBoxSDS            VirtualBox system service
Stopped  vds                Disque virtuel
Running  VMAuthdService     VMware Authorization Service
Stopped  vmicguestinterface Interface de services d’invité Hyper-V
Stopped  vmicheartbeat      Service Pulsation Microsoft Hyper-V
Stopped  vmickvpexchange    Service Échange de données Microsof...
Stopped  vmicrdv            Service de virtualisation Bureau à ...
Stopped  vmicshutdown       Service Arrêt de l’invité Microsoft...
Stopped  vmictimesync       Service Synchronisation date/heure ...
Stopped  vmicvmsession      Service Hyper-V PowerShell Direct
Stopped  vmicvss            Requête du service VSS Microsoft Hy...
Running  VMnetDHCP          VMware DHCP Service
Running  VMUSBArbService    VMware USB Arbitration Service
Running  VMware NAT Service VMware NAT Service
Stopped  VSS                Cliché instantané des volumes
Stopped  W32Time            Temps Windows
Stopped  w3logsvc           Service de journalisation W3C
Stopped  WaaSMedicSvc       Windows Update Medic Service
Stopped  WalletService      WalletService
Stopped  WarpJITSvc         WarpJITSvc
Stopped  WAS                Service d'activation des processus ...
Stopped  wbengine           Service de moteur de sauvegarde en ...
Running  WbioSrvc           Service de biométrie Windows
Running  Wcmsvc             Gestionnaire des connexions Windows
Stopped  wcncsvc            Windows Connect Now - Registre de c...
Running  WdiServiceHost     Service hôte WDIServiceHost
Running  WdiSystemHost      Hôte système de diagnostics
Stopped  WdNisSvc           Service Inspection du réseau de l’a...
Stopped  WebClient          WebClient
Stopped  Wecsvc             Collecteur d’événements de Windows
Stopped  WEPHOSTSVC         Service hôte du fournisseur de chif...
Stopped  wercplsupport      Prise en charge de l’application Ra...
Stopped  WerSvc             Service de rapport d’erreurs Windows
Stopped  WFDSConMgrSvc      Service Wi-Fi Direct Service de ges...
Stopped  WiaRpc             Événements d’acquisition d’images f...
Stopped  WinDefend          Service antivirus Windows Defender
Running  WinHttpAutoProx... Service de découverte automatique d...
Running  Winmgmt            Infrastructure de gestion Windows
Stopped  WinRM              Gestion à distance de Windows (Gest...
Stopped  wisvc              Service Windows Insider
Running  WlanSvc            Service de configuration automatiqu...
Running  wlidsvc            Assistant Connexion avec un compte ...
Stopped  wlpasvc            Service de l’Assistant de profil local
Stopped  WManSvc            Service de gestion de Windows
Stopped  wmiApSrv           Carte de performance WMI
Stopped  WMPNetworkSvc      Service Partage réseau du Lecteur W...
Stopped  workfolderssvc     Dossiers de travail
Stopped  WpcMonSvc          Contrôle parental
Stopped  WPDBusEnum         Service Énumérateur d’appareil mobile
Running  WpnService         Service du système de notifications...
Running  WpnUserService_... WpnUserService_436cd
Running  wscsvc             Centre de sécurité
Running  WSearch            Windows Search
Running  wuauserv           Windows Update
Stopped  WwanSvc            Service de configuration automatiqu...
Stopped  XblAuthManager     Gestionnaire d'authentification Xbo...
Stopped  XblGameSave        Jeu sauvegardé sur Xbox Live
Stopped  XboxGipSvc         Xbox Accessory Management Service
Stopped  XboxNetApiSvc      Service de mise en réseau Xbox Live
Running  ZeroConfigService  Intel(R) PROSet/Wireless Zero Confi...
```

### script

Ils sont push sur le git.

### Gestion de softs

Le gestionnaire de paquets est un système qui permet d'installer des logiciels, de les maintenir à jour et de les désinstaller. Son travail est de n'utiliser que des éléments compatibles entre eux, les installations sans utiliser de gestionnaire de paquets sont donc déconseillées.

Je suis sous windows. J'installe donc chocoltey qui est un gestionnaire de paquet windows, avec la commande `Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))` en mode administrateur.

Ensuite pour lister tous les paquets déjà installés. On fait la commande `choco list` et j'obtiens : 

```
PS C:\WINDOWS\system32> choco list
Chocolatey v0.10.15
azshell 0.2.2 [Approved] Downloads cached for licensed users
tusk.portable 0.23.0 [Approved]
corsixth 0.63 [Approved]
playlist-creator 3.6.2 [Approved] - Possibly broken
moonlight.install 1.0.0 [Approved]
playlist-creator.portable 3.6.2 [Approved] Downloads cached for licensed users - Possibly broken for FOSS users (due to original download location changes by vendor)
playlist-creator.install 3.6.2 [Approved] Downloads cached for licensed users - Possibly broken for FOSS users (due to original download location changes by vendor)
corsixth.portable 0.63 [Approved]
k-meleon.portable 75.1 [Approved]
k-meleon.install 75.1 [Approved]
k-meleon 75.1 [Approved]
moonlight.portable 1.0.0 [Approved]
brutaldoom-scythe2 2.0.0 [Approved] Downloads cached for licensed users
brutaldoom-dtwid 1.1.1 [Approved] Downloads cached for licensed users
brutaldoom-goingdown 1.0.0 [Approved] Downloads cached for licensed users
brutaldoom-d2reload 1.0.0 [Approved] Downloads cached for licensed users
3dduke-shareware 1.3.0 [Approved] Downloads cached for licensed users
opentyrian 2.1.0 [Approved] Downloads cached for licensed users
infinitex.portable 0.9.16 [Approved]
infinitex 0.9.16 [Approved]
infinitex.install 0.9.16 [Approved]
doom-sigil 1.1.0 [Approved] Downloads cached for licensed users
spaceradar 5.1.0 [Approved] Downloads cached for licensed users
dotmemory-unit 3.0.20171219.105559 [Approved]
keepass-kpentrytemplates 1.8.0.20190608 [Approved]
speedcrunch.portable 0.12 [Approved]
nats-server 2.0.0.20190610 [Approved] Downloads cached for licensed users
pluralsight-transcripter 1.0.0 [Approved] Downloads cached for licensed users
ukinternationalkeyboard 2.0.0 [Approved] Downloads cached for licensed users
klatexformula 4.0.0 [Approved]
boot-no-hyperv 0.1.0 [Approved]
classyshark 8.2 [Approved]
KB2519277 1.0.0 [Approved] Downloads cached for licensed users
dexed 0.9.4 [Approved]
rss-builder 2.1.8 [Approved] Downloads cached for licensed users
qaac 2.68 [Approved] Downloads cached for licensed users
avinaptic 2011.12.18 [Approved]
splint.portable 3.1.2 [Approved]
splint.install 3.1.2 [Approved]
splint 3.1.2 [Approved]
mcr-r2015b 9.0 [Approved] Downloads cached for licensed users
q10.portable 1.2.21 [Approved]
aerozoom 4.0.0.7 [Approved]
fluxfonts 2.0 [Approved]
windows-sandbox 0.0.1 [Approved]
dissenter-browser 0.66.99 [Approved] Downloads cached for licensed users
altcopy 1.0.7 [Approved] Downloads cached for licensed users
gsubs 1.0.2 [Approved]
quollwriter 2.6.14 [Approved]
brutaldoom-kamasutra 03.09.05 [Approved] Downloads cached for licensed users
click-monitor-ddc 7.0 [Approved] Downloads cached for licensed users
buddybackup 2.0.2.400 [Approved] Downloads cached for licensed users
prometheus-grok-exporter 0.2.8 [Approved] Downloads cached for licensed users
cica 5.0.1.20190723 [Approved] Downloads cached for licensed users
librefox-firefox 2.1 [Approved]
vscode-cortex-debug 0.3.1 [Approved]
cronical 1.3 [Approved]
little-fighter-2 2.0 [Approved] Downloads cached for licensed users
jcliff 2.12.3 [Approved]
mcr-r2019a 9.6.0.1150989 [Approved] Downloads cached for licensed users
wop 1.6 [Approved] Downloads cached for licensed users
keyferret.install 2.6 [Approved]
keyferret.portable 2.6 [Approved]
keyferret 2.6 [Approved]
sql-multi-select 4.3.0.142 [Approved] Downloads cached for licensed users - Possibly broken for FOSS users (due to original download location changes by vendor)
tabula 1.2.1 [Approved]
watchexec 1.10.3 [Approved] Downloads cached for licensed users
acronis-vdi 1.1.2141.20190805 [Approved] Downloads cached for licensed users
disable-logon-blur 1.0.0 [Approved]
zephyr-ide 1.14.0 [Approved]
acronis-drive-monitor 1.0.566 [Approved] Downloads cached for licensed users
startup-organizer 2.9.324 [Approved] Downloads cached for licensed users
evga-eleet 1.10.4 [Approved] Downloads cached for licensed users
hyperx-ngenuity 5.2.8.0 [Approved] Downloads cached for licensed users
gpac 0.8.0.20190812 [Approved] Downloads cached for licensed users
briss 2.0 [Approved]
huggle 3.4.9 [Approved] Downloads cached for licensed users
adoptopenjdk12 12.0.2.10 [Approved] Downloads cached for licensed users
tcpipmanager.portable 4.1.1 [Approved]
tcpipmanager.install 4.1.1 [Approved]
leagueoflegendseuw 1.0.0.0 [Approved] Downloads cached for licensed users - Possibly broken for FOSS users (due to original download location changes by vendor)
tcpipmanager 4.1.1 [Approved]
orangec 6.0.42.1 [Approved]
jpdftweak 1.1 [Approved]
nip2 8.7.0 [Approved]
paraview 5.6.1 [Approved] Downloads cached for licensed users
prometheus-sql-exporter 0.5 [Approved] Downloads cached for licensed users
sdcard-formatter 5.0.1 [Approved] Downloads cached for licensed users
iecv 1.79 [Approved]
dotmemory-console 2019.2.1 [Approved]
90 packages found.
```

